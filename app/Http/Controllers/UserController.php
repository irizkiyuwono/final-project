<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function export() 
    {
        //this function is for laravel excel
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
