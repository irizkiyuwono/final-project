@extends('template')

@section('content')
    <!--- \\\\\\\Post-->
   <section>
       <button type="button" data-toggle="collapse" class="btn btn-primary container-fluid" data-target="#createstatus" > post status</button>
    <div class="card collapse" id="createstatus">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                        a publication</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Images</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                    <div class="form-group">
                        <label class="sr-only" for="message">Post</label>
                        <textarea class="form-control" id="message" rows="3" placeholder="What are you thinking?"></textarea>
                    </div>

                </div>
                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Upload image</label>
                        </div>
                    </div>
                    <div class="py-4"></div>
                </div>
            </div>
            <div class="btn-toolbar justify-content-between">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Share</button>
                </div>
                <div class="btn-group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fa fa-globe"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="card bg-light mt-1">
        <div class="card-header">
            <div class="d-flex justify-content-left">
                <div class="mr-2">
                    <img class="rounded-circle" width="45" src="https://picsum.photos/50/50" alt="">
                </div>
                <div class="ml-2">
                    <!-- nanti ini di link -->
                    <div class="h5 m-0">UserName</div>
                    <div class="h7 text-muted">Miracles Lee Cross</div>
                </div>
            </div>
        </div>
        <div class="card-body">
            {{-- if img !=== null--}}
            @if (2 === 1)
                <img src="" alt="" class="">
            @endif
            <!-- text dibawah ini juga nanti di link -->
            <p class="card-text">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam omnis nihil, aliquam est, voluptates officiis iure soluta
                alias vel odit, placeat reiciendis ut libero! Quas aliquid natus cumque quae repellendus. Lorem
                ipsum dolor sit amet consectetur adipisicing elit. Ipsa, excepturi. Doloremque, reprehenderit!
                Quos in maiores, soluta doloremque molestiae reiciendis libero expedita assumenda fuga quae.
                Consectetur id molestias itaque facere? Hic!
            </p>
        </div>

        <div class="card-footer d-flex">
            <button  class="btn btn-light"><i class="fa fa-gittip"></i> Like</button>
            <button  class="btn btn-light"><i class="fa fa-mail-forward"></i> dislike</button>
            <button  class="btn btn-light" data-toggle="collapse" data-target="#coment"><i class="fa fa-comment"></i> Comment</button>

        </div>

        <div class="collapse" id="coment">
            <form action="">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="coment" id="coment" name="coment">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
        </div>

    </div>
    <!-- Post /////-->
   </section>

@endsection

@push('scripts')
<script>
    //
</script>
@endpush
