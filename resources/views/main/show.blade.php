@extends('template')

@section('content')

<style>
.work-feature-block {
  padding: 10px 10px;
}

.work-feature-block-image {
  display: block;
  margin: auto;
  padding: 10px 0;
}

.button-hover-like {
  border: 2px solid #767676;
  background: transparent;
  color: #767676;
  text-transform: uppercase;
  overflow: hidden;
  letter-spacing: 0.07rem;
  transition: all 0.2s ease-in-out;
  position: relative;
}

.button-hover-like span {
  transition: all 0.2s ease-in-out;
}

.button-hover-like .bx {
  position: absolute;
  font-size: 1.2rem;
  top: 50%;
  -webkit-transform: translateY(-50%);
      -ms-transform: translateY(-50%);
          transform: translateY(-50%);
  color: #F15261;
  right: -20px;
  transition: 0.4s right cubic-bezier(0.38, 0.6, 0.48, 1);
}

.button-hover-like:hover {
  border-color: #1779ba;
  background: transparent;
  transition: border-color 0.2s;
}

.button-hover-like:hover span {
  margin-right: 20px;
  color: #1779ba;
}

.button-hover-like:hover .bx {
  right: 5px;
}

.button-hover-like:active {
  transition: all 0.05s ease-in-out;
  background-color: #1779ba;
}

.button-hover-like:active span {
    transition: all 0.05s ease-in-out;
    color: #FFF;

}

.wrapper-user {
    display: inline-flex;
    align-content: center;
    justify-content: space-between;
    width: 50%;
}

</style>

<div class="container ml-5 mt-5">
    <div class="work-feature-block row">
    <div class="columns medium-7">
      <!-- image will be filled with inputted image -->
      <img class="work-feature-block-image" src="https://placehold.it/600x400"/>
    </div>
    <div class="columns medium-5">
        <!-- this form below is for like button -->
        <form class="wrapper-user columns medium-5" role="form" action="" method="POST"> 
            <button type="submit" id="btn" class="button-hover-like button"><span>Like me</span><i class="bx bxs-heart"></i></button>
            <h6><!-- Original Poster name--> <em>poster</em> : --poster--</h6>
        </form>
        <h2 class="work-feature-block-header mt-3">Project Description</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales diam ac hendrerit aliquet. Phasellus pretium libero vel molestie maximus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis est quam. Aenean blandit a urna laoreet tincidunt.</p>
        <h2>Project Details</h2>
        <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
        </ul>
    </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    //
</script>
@endpush